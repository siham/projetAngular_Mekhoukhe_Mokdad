/**
 * Created by acer on 28/11/2016.
 */

exports.cleanRes = function(res){

    var out=[], obj = {}, i,j, len=res.length;
    for ( var i=0; i < len; i++) {
        obj[res[i]] = 0;
    }
    for (j in obj) {
        out.push(j);
    }
    return out;
}
