var mesFonction = require("./mesFonction.js");
var MongoClient = require('mongodb').MongoClient;
var assert = require('assert');
var ObjectId = require('mongodb').ObjectID;
var url = 'mongodb://localhost:27017/VentesEnLignes';
express  = require("express");
var app = express();

/*
 Nom de la base : VentesEnLignes
 Nom de la collection : Produits
 Documents : [{'type': 'téléphone', 'marque':'Apple', 'nom':'iphone'}, {'type': 'téléphone', 'marque':'Samsung', 'nom':'S7'}]
 */

MongoClient.connect(url, function(err, database) {
    if (err) throw err;
    db = database;
    app.listen(8989);
    console.log("i am in the server");
});

//recuperation des marques sans filtre (appel du composant1)
app.get("/produits/marques", function(req, res) {
    console.log("Recherches sur les marques");
    rechercheProduitsMarque(db, {}, function(json) {
        console.log(json);
        res.setHeader('Access-Control-Allow-Origin', '*');
        res.setHeader('Content-Type', 'application/json');
        res.end(JSON.stringify(json));
    });

});
//callback du composant1 recherche par marque sans doublons
var rechercheProduitsMarque = function(db, search, callback) {
    var cursor = db.collection('produits').find( search );
    var res = [];
    var i = 0;
    cursor.each(function(err, doc) {
        assert.equal(err, null);
        if (doc != null) {
            res.push(doc);
            console.log(res);
            i++;}

        else { callback(res); }
    });
};

//recuperation des types sans filtre (appel du composant2)
app.get("/produits/types", function(req, res) {
    console.log("je prends que les types");
    rechercheType(db, {}, function(json) {
        console.log(json);
        res.setHeader('Access-Control-Allow-Origin', '*');
        res.setHeader('Content-Type', 'application/json');
        res.end(JSON.stringify(json));
    });
});

//callback du composant2 recherche par type sans doublons
var rechercheType = function (db, search, callback) {
    var cursor = db.collection('produits').find(search);
    var res = [];
    var i = 0;
    cursor.each(function (err, doc) {
        assert.equal(err, null);
        console.log(doc);
        if (doc != null) {
            console.log(res.length);
            //var marque = doc['marque'];
            var type = doc.type;
            console.log(type);
            var type = doc.type;
            res.push(type);
            var newRes = mesFonction.cleanRes(res);
            console.log("mon res ");
            res = newRes;
            console.log(res);

            i++;
            console.log(i);

        }
        else {
            callback(res);
        }
    });
};


//Recherche marque avec parametre (composant3)
app.get("/produits/marque/:marque", function(req, res) {
    var marqueARechercher = req.params.marque;
    console.log("Marque à rechercher : "+marqueARechercher);
    rechercheProduits(db, {"marque":marqueARechercher}, function(json) {
        console.log(json);
        res.setHeader('Access-Control-Allow-Origin', '*');
        res.setHeader('Content-Type', 'application/json');
        res.end(JSON.stringify(json));
    });
});

var rechercheProduits = function(db, search, callback) {
    var cursor = db.collection('produits').find( search );
    var res = [];
    var i = 0;
    cursor.each(function(err, doc) {
        assert.equal(err, null);
        if (doc != null){
            console.log(doc);
            res.push(doc);
            console.log(res);
            i++;}

        else { callback(res); }
    });
};





