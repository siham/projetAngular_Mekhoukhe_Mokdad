import {Component, OnInit} from '@angular/core';
import {Http, Response} from '@angular/http';
import 'rxjs/add/operator/map';

@Component({
    selector:    'composant2',
    templateUrl: 'templates/composant2.html',
    styleUrls:   ['styles/composant2.css']
})
export class Composant2Component {
    private http: Http;
    public items :any;

    public constructor(http :Http) {
       this.http = http;
    }

    ngOnInit() {
       console.log('Appel du serveur pour importer les types des produits');

       this.http.get('http://localhost:8989/produits/types')
                     .map((res:Response) => res.json())
                     .subscribe(res => this.items = res,
                                err => console.error(err),
                                () => console.log('done') );
  }
}