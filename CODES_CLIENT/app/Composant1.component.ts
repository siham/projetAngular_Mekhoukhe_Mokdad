import {Component, OnInit} from '@angular/core';
import {Http, Response} from '@angular/http';
import {Router} from '@angular/router';
import 'rxjs/add/operator/map';

@Component({
    selector:    'composant1',
    templateUrl: 'templates/composant1.html',
    styleUrls:   ['styles/composant1.css']
})
export class Composant1Component {
    private http: Http;
    private router :Router;
    public items :any;
    public marque :string;

    public constructor(http :Http, router: Router) {
       this.http = http;
       this.router = router;
    }

    ngOnInit() {
           console.log("Recherche de toutes les marques");
           this.http.get('http://localhost:8989/produits/marques')
                     .map((res:Response) => res.json())
                     .subscribe(res => this.items = res,
                                err => console.error(err),
                                () => console.log('done') );
    }

    public appelComposant3(parametre : string) {
           let link = ['/composant3', parametre];
           this.router.navigate(link);
    }

}