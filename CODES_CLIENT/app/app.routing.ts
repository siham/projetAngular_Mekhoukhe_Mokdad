import { ModuleWithProviders }  from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { Composant1Component }  from './Composant1.component';
import { Composant2Component }  from './Composant2.component';
import { Composant3Component }  from './Composant3.component';
import { Composant4Component }  from './Composant4.component';

import {MenuComponent} from "./Menu.component";

export const appRoutes: Routes = [
    {path: '', redirectTo: 'menu', pathMatch: 'full'},
  { path: 'composant1', component: Composant1Component },
  { path: 'composant3/:marque', component: Composant3Component },
  { path: 'composant2', component: Composant2Component },
    { path: 'composant4',component: Composant4Component},

];

export const appRoutingProviders: any[] = [ ];

export const routing: ModuleWithProviders = RouterModule.forRoot(appRoutes);
