import {Component} from '@angular/core';

@Component({
    selector:    'menu',
    templateUrl: 'templates/menu.html',
    styleUrls:   ['styles/menu.css']
})
export class MenuComponent {
    titre = 'Vente En Ligne';
    marque :string = 'Apple';

    setMarque( value :string) {
      console.log("Dans Menu avec "+value);
      this.marque = value;
   }
}
