"use strict";
var router_1 = require('@angular/router');
var Composant1_component_1 = require('./Composant1.component');
var Composant2_component_1 = require('./Composant2.component');
var Composant3_component_1 = require('./Composant3.component');
var Composant4_component_1 = require('./Composant4.component');
exports.appRoutes = [
    { path: '', redirectTo: 'menu', pathMatch: 'full' },
    { path: 'composant1', component: Composant1_component_1.Composant1Component },
    { path: 'composant3/:marque', component: Composant3_component_1.Composant3Component },
    { path: 'composant2', component: Composant2_component_1.Composant2Component },
    { path: 'composant4', component: Composant4_component_1.Composant4Component },
];
exports.appRoutingProviders = [];
exports.routing = router_1.RouterModule.forRoot(exports.appRoutes);
//# sourceMappingURL=app.routing.js.map