import {Component, OnInit} from '@angular/core';
import {Http, Response} from '@angular/http';
import {Router} from '@angular/router';
import 'rxjs/add/operator/map';

@Component({
    selector:    'composant1',
    templateUrl: 'templates/composant4.html',
    styleUrls:   ['styles/composant1.css']
})
export class Composant4Component {
    private http: Http;
    private router: Router;
    public items: any;
    public marque: string;

    public constructor(http: Http, router: Router) {
        this.http = http;
        this.router = router;
    }
}