import { NgModule }      from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule }   from '@angular/forms';
import { HttpModule }    from '@angular/http';

import { routing, appRoutingProviders } from './app.routing';

import { MenuComponent }        from './Menu.component';
import { Composant1Component }  from './Composant1.component';
import { Composant2Component }  from './Composant2.component';
import { Composant3Component }  from './Composant3.component';
import { Composant4Component }  from './Composant4.component';


@NgModule({
  imports:      [ BrowserModule, FormsModule, HttpModule, routing ],
  declarations: [ MenuComponent, Composant1Component, Composant2Component, Composant3Component, Composant4Component ],
  providers:    [ appRoutingProviders ],
  bootstrap:    [ MenuComponent ]
})
export class AppModule { }
